package com.gwz.shelf.consts;


/**
 * redis key
 */
public class RedisKeyConst {
    public final static String AUTH = "AUTH_";
    public final static String IMG_CODE = "IMG_CODE_";
}

package com.gwz.shelf.consts;

public enum ResponseCode {
    success(200, "成功"),
    faill(500, "失败"),
    tokenInvalidation(501, "token失效"),
    failedToObtainUserInformation(502, "获取用户信息失败"),
    ;
    public int code;
    public String message;

    ResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}

package com.gwz.shelf.mapper.auth;

import com.gwz.shelf.entity.auth.bo.Modules;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模块表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface ModulesMapper extends BaseMapper<Modules> {

}

package com.gwz.shelf.mapper.auth;

import com.gwz.shelf.entity.auth.bo.Roles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface RolesMapper extends BaseMapper<Roles> {

}

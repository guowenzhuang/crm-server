package com.gwz.shelf.mapper.auth;

import com.gwz.shelf.entity.auth.bo.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}

package com.gwz.shelf.mapper.auth;

import com.gwz.shelf.entity.auth.bo.LogginPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登录日志表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface LogginPermissionMapper extends BaseMapper<LogginPermission> {

}

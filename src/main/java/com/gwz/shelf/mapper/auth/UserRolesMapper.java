package com.gwz.shelf.mapper.auth;

import com.gwz.shelf.entity.auth.bo.UserRoles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface UserRolesMapper extends BaseMapper<UserRoles> {

}

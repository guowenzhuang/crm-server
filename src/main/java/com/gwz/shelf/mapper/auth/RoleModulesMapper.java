package com.gwz.shelf.mapper.auth;

import com.gwz.shelf.entity.auth.bo.RoleModules;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色模块表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface RoleModulesMapper extends BaseMapper<RoleModules> {

}

package com.gwz.shelf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwz.shelf.entity.auth.bo.UserRoles;
import com.gwz.shelf.mapper.auth.UserRolesMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class UserRolesService extends ServiceImpl<UserRolesMapper, UserRoles> implements IService<UserRoles> {

}

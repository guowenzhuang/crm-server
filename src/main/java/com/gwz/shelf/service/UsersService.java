package com.gwz.shelf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwz.shelf.entity.auth.bo.Users;
import com.gwz.shelf.mapper.auth.UsersMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class UsersService extends ServiceImpl<UsersMapper, Users> implements IService<Users> {

}

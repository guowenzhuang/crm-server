package com.gwz.shelf.service;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gwz.shelf.entity.auth.bo.Modules;
import com.gwz.shelf.entity.auth.bo.Permission;
import com.gwz.shelf.entity.auth.bo.RoleModules;
import com.gwz.shelf.entity.auth.bo.RolePermission;
import com.gwz.shelf.mapper.auth.PermissionMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class PermissionService extends ServiceImpl<PermissionMapper, Permission> implements IService<Permission> {

    @Autowired
    private RolesService rolesService;
    @Autowired
    private RolePermissionService rolePermissionService;

    /**
     * @return
     */
    public List<Permission> getPermissionByUserId(String userId) {
        List<String> roleIds = rolesService.getByUserId(userId);
        List<String> permissionId = rolePermissionService
                .lambdaQuery()
                .in(RolePermission::getRoleId, roleIds)
                .list()
                .stream()
                .map(RolePermission::getPermissionId)
                .collect(Collectors.toList());
        return lambdaQuery().in(Permission::getId, permissionId).list();
    }
}

package com.gwz.shelf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwz.shelf.entity.auth.bo.Roles;
import com.gwz.shelf.entity.auth.bo.UserRoles;
import com.gwz.shelf.mapper.auth.RolesMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class RolesService extends ServiceImpl<RolesMapper, Roles> implements IService<Roles> {
    @Autowired
    private UserRolesService userRolesService;

    /**
     * 根据用户id查询所有角色Id
     *
     * @param userId
     * @return
     */
    public List<String> getByUserId(String userId) {
        return userRolesService
                .lambdaQuery()
                .eq(UserRoles::getUserId, userId)
                .list()
                .stream()
                .map(UserRoles::getRoleId)
                .collect(Collectors.toList());
    }
}

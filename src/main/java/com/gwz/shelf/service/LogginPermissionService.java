package com.gwz.shelf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwz.shelf.entity.auth.bo.LogginPermission;
import com.gwz.shelf.mapper.auth.LogginPermissionMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录日志表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class LogginPermissionService extends ServiceImpl<LogginPermissionMapper, LogginPermission> implements IService<LogginPermission> {

}

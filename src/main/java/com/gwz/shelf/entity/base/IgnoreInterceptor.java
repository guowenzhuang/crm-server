package com.gwz.shelf.entity.base;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestMethod;

@Builder
@Data
public class IgnoreInterceptor {
    private String url;
    private RequestMethod method;
}

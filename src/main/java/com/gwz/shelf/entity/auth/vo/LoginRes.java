package com.gwz.shelf.entity.auth.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginRes {
    @ApiModelProperty(value = "token", required = true)
    private String token;
}

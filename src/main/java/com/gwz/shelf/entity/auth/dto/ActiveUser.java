package com.gwz.shelf.entity.auth.dto;

import cn.hutool.core.lang.tree.Tree;
import com.gwz.shelf.entity.auth.bo.Permission;
import com.gwz.shelf.entity.auth.bo.Users;
import lombok.Data;

import java.util.List;

@Data
public class ActiveUser {
    private Users users;
    private List<Tree<String>> menu;
    private  List<Permission> permissions;
}

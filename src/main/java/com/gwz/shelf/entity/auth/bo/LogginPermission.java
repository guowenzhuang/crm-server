package com.gwz.shelf.entity.auth.bo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 登录日志表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName
@Data
public class LogginPermission  extends Model<LogginPermission> {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 用户id
     */
    private String userId;

    /**
     * 内容
     */
    private String comment;

    private Integer isSuccess;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}

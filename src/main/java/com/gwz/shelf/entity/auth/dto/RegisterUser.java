package com.gwz.shelf.entity.auth.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RegisterUser {
    /**
     * 名称
     */
    @NotBlank
    @ApiModelProperty(value = "账号", required = true)
    private String name;
    /**
     * 密码
     */
    @NotBlank
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    /**
     * 图形id
     */
    @NotBlank
    @ApiModelProperty(value = "图形id 获取图形验证码接口返回", required = true)
    private String imgId;
    /**
     * 图形验证码
     */
    @NotBlank
    @ApiModelProperty(value = "用户输入的图形验证码", required = true)
    private String imgCode;

}

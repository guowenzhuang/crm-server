package com.gwz.shelf.entity.auth.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ImgCode {
    @ApiModelProperty(value = "base64Img", required = true)
    private String img;
    @ApiModelProperty(value = "imgId 登录需要用到", required = true)
    private String imgId;
}
